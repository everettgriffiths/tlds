CHANGELOG
=========

1.1.2 (2014-10-03)
------------------

* fix autoload-dev in composer.json, should be pointing to tests/
* tidy up Service Provider - use bindShared function
* clean up old unit tests in ValidatorExtensionsTest

1.1.1 (2014-10-03)
------------------

* update package dependency for hampel/validate to ~2.2

1.1.0 (2014-10-03)
------------------

* added validation extensions to validate TLDs and domain names against the TLD list

1.0.0 (2014-10-03)
------------------

* initial release
